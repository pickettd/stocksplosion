# Project Setup and Environment

This project uses Rails on the backend and Angular.js for the frontend. A Vagrantfile is provided to aid in environment setup with an aim towards dev/prod parity (if vagrant-aws were to be used in production for example).

Once Vagrant ([install](https://docs.vagrantup.com/v2/installation/)) and Virtualbox ([download](https://www.virtualbox.org/wiki/Downloads)) are installed, cloning the repo and running `vagrant up` from the project root will configure the VM with all prerequisites, share port 3000 between host and VM, and start the Rails server. On the host machine you can just aim a browser at localhost:3000 to reach the server. You can stop the VM with the command `vagrant halt` and if you run `vagrant up` again it will resume the VM and the application server without having to do a fresh install again. Note - if you are using Windows as a host machine and run into issues with setup, it might be helpful to run the vagrant commands from an elevated command prompt (Virtualbox on Windows needs elevated access in order to make symlinks work properly in synced folders).

You can review the shell provisioning instructions in the Vagrantfile if you'd like to run the project without using Vagrant. The summary is 1) install Ruby > 2.0 then `gem install bundler` using Ruby 2.0's gem, 2) install and configure postgresql with a user and password with permissions to create databases, 3) set the environment variables STOCKSPLOSION_DATABASE_ROLE and STOCKSPLOSION_DATABASE_PASSWORD to the PG username and password respectively, 4) run `bundle install` from the project root, 5) run `bundle exec rake db:create`, and finally 6) `bundle exec rails s` to start the application server.

### Testing
There is a partial set of unit tests for the primary Angular controller in spec/javascripts written using Jasmine and Teaspoon. The test suite can be run in the Vagrant VM by executing `rake teaspoon` in the project root folder after a `vagrant ssh` connection.

### Deployed production environment demo
The project has been deployed as a demo to http://nameless-fjord-4245.herokuapp.com/ to test production configuration and provide developers an example of the app running. Note that the company API is http only so you must access the heroku link with http and not https to avoid connection errors.

# Project Description
Work sample project for Apsis Labs based on the fictional client company STOCKSPLOSION:

STOCKSPLOSION wants to create an application that will provide analysts with a quick decision on whether or not to purchase a given stock. The application should be able to:

1. Let the user submit the ticker symbol for a company's stock.
2. Display a summary for that stock's recent performance.
3. Provide the analyst with one of three options: Buy, Wait, or Sell. The specifics of how that decision is made are up to you. 

# Next Steps / Future Improvements
* The current algorithm to determine stock buy rating is too conservative - it almost always advises waiting. By combining multiple indicators and plotting trendlines it should be possible to drastically increase the sensitivity of the application to market conditions.
* Better test coverage - currently there are no end-to-end tests and not full coverage from unit tests.
* The provisioning system could be made more robust by using a tool like Chef instead of shell provisioning.
* More user controls could be included in the interface for an analyst to explore older data, multiple stocks next to each other, or to tweak indicator parameters manually.
* Could improve performance by storing trading indicator calculations instead of making them on demand.
* Angular Google Chart library doesn't render properly on Android in Chrome currently. But works on iOS in Safari and Chrome - so the issue needs more investigation to resolve.
