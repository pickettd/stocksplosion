require 'indicators'

class MainController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def index
  end
  def rsi
    data_hash = params[:prices]
    transFormed_prices = []
    for (date, price) in data_hash
      thisEntry = {}
      thisEntry[:date] = date
      thisEntry[:adj_close] = price
      transFormed_prices.push(thisEntry)
    end

    my_data = Indicators::Data.new(transFormed_prices)
    my_rsi = my_data.calc(:type => :rsi, :params => 5)

    @rsi = {symbol: params[:symbol], latest_rsi: my_rsi.output[my_rsi.output.length-1]}
  end
end
