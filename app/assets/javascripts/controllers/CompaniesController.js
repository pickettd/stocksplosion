controllers = angular.module('controllers');

controllers.controller("CompaniesController", [ '$scope', '$routeParams', '$location', '$resource', 'CompaniesFactory',
  function($scope,$routeParams,$location, $resource, CompaniesFactory) {
    /*
    When search is called, navigate to the state with that symbol in the URL
    */
    $scope.search = function(symbol) {
      $location.path("/").search('symbol', symbol);
    };

    var updateScope = function() {
        $scope.companies = CompaniesFactory.companiesResult;
        $scope.headerMessage = CompaniesFactory.headerMessage;
    };

    $scope.headerMessage = CompaniesFactory.headerMessage;
    $scope.companies = CompaniesFactory.companiesResult;

    if ($routeParams.symbol) {
      /*
      This is the case of a single company being requested. If it is not found an error message will show.
      */
      symbol = $routeParams.symbol.toLowerCase();
      var endDate = new Date();
      var startDate = new Date(endDate);
      startDate.setDate(endDate.getDate() - 30);
      /*
      Using finally makes sure that the scope gets updated in the case of success or failure.
      */
      CompaniesFactory.getOneCompany($routeParams.symbol, startDate, endDate).finally(function() {
        updateScope();
        /*
        Since this is a finally block - need to check that there really is data to chart
        */
        if (CompaniesFactory.companiesResult) {
          $scope.chartObject.data = {"cols": [
            {id: "t", label: "Date", type: "date"},
            {id: "s", label: "Adjusted Closing Price", type: "number"}
          ], "rows": CompaniesFactory.companiesResult[0].priceRows};

          $scope.chartObject.options = {
            'title': '30 day performance of '+$routeParams.symbol
          };
        }
      });
    }
    /*
    If $routeParams.symbol is undefined then we are just at the home page and shouldn't query the server yet.
    If $routeParams.symbol is empty then the See All button was pressed and we should get the full company list.
    */
    if ($routeParams.symbol === '') {
      /*
      This is the case of all companies being requested. If the API call has a failure and error message will show.
      */
      CompaniesFactory.getAllCompanies().finally(updateScope);
    }
    $scope.chartObject = {};

    $scope.chartObject.data = {"cols": [
        {id: "t", label: "Date", type: "date"},
        {id: "s", label: "Adjusted Closing Price", type: "number"}
    ], "rows": [
        {c: [
            {v: new Date(2015,01,01)},
            {v: 16928.27},
        ]}
    ]};

    // $routeParams.chartType == BarChart or PieChart or ColumnChart...
    $scope.chartObject.type = "LineChart";
    $scope.chartObject.options = {};
  }]);
