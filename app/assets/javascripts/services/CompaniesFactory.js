services = angular.module('services');

services.factory('CompaniesFactory', ['$resource', '$http', function ($resource, $http) {
  var CompaniesFactory = {};
  CompaniesFactory.headerMessage = '';
  CompaniesFactory.companiesResult = null;
  /*
  The pad function is used to make sure that month or day values below 10 have an extra 0 prepended in the string representation
  */
  var pad = function (number) {
    if (number < 10) {
      return '0' + number;
    }
    return number;
  };

  var dateStringFromDate = function (inputDate) {
    var returnString = "";
    returnString += inputDate.getFullYear();
    /*
    getMonth returns a value from 0 to 11, so add one for the return string
    */
    returnString += pad(inputDate.getMonth()+1);
    returnString += pad(inputDate.getDate());
    return returnString;
  };

  var dateFromDateString = function (inputString) {
    var year = inputString.substring(0, 4);
    var month = parseInt(inputString.substring(4, 6))-1;
    var day = inputString.substring(6, 8);
    var returnDate = new Date(year, month, day);
    return returnDate;
  };

  /*
  This function defines the RSI thresholds for determining if a stock should get a buy, sell, or wait rating.
  */
  var decideRatingFromLatestRSI = function (latest_rsi) {
    var returnRating = "";
    if (latest_rsi <= 40)
      returnRating = "Buy";
    else if (latest_rsi >= 60)
      returnRating = "Sell";
    else
      returnRating = "Wait";

    return returnRating;
  };

  OneCompany = $resource('http://stocksplosion.apsis.io/api/company/:companySymbol',{ companySymbol: "@symbol" }, {
    /*
    Query is specially defined here because the server responds with a dictionary (instead of a plain array)
    */
    query: { method: 'GET' }}
  );
  AllCompanies = $resource('http://stocksplosion.apsis.io/api/company', {});

  RSICompany = $resource('/rsi', { companySymbol: "@symbol" }, {
    /*
    Query is specially defined here because the server responds with a dictionary (instead of a plain array)
    */
    query: { method: 'GET' }}
  );

  CompaniesFactory.getAllCompanies = function() {
    CompaniesFactory.headerMessage = 'All Companies';
    return AllCompanies.query({},
        /*
        Success callback
        */
        function(results) {
          CompaniesFactory.allCompanies = results;
          CompaniesFactory.companiesResult = results;
      },
        /*
        Failure callback
        */
        function(error) {
          CompaniesFactory.companiesResult = null;
          CompaniesFactory.headerMessage = "Could not access company list at this time, please try again later";
      }).$promise;
  };

  CompaniesFactory.getOneCompany = function(symbol, startDate, endDate) {
    CompaniesFactory.headerMessage = 'Information';
    return OneCompany.query({ companySymbol: symbol, startdate: dateStringFromDate(startDate), enddate: dateStringFromDate(endDate) },
        /*
        This function defines the success route of the API call.
        */
        function(result) {
          /*
          Make a post request to the rsi service based on price data
          */
          $http.post('/rsi', {format: 'json', symbol: symbol, prices: result.prices}).
            success(function(data, status, headers, config) {
              if (CompaniesFactory.companiesResult[0].symbol === data.symbol) {
                CompaniesFactory.companiesResult[0].rating = decideRatingFromLatestRSI(data.latest_rsi);
              }
            }).
            error(function(data, status, headers, config) {
              // called asynchronously if an error occurs
              // or server returns response with an error status.
            });
          var rows = [];
          result.company.rating = "<Calculating>";
          angular.forEach(result.prices, function(price, dateString) {
            var dateValue = dateFromDateString(dateString);
            var cols = [];
            cols.push({v: dateValue});
            cols.push({v: price});
            rows.push({c: cols});
          });
          result.company.priceRows = rows;
          CompaniesFactory.companiesResult = [result.company];
        },
        /*
        This function defines the failure route of the API call.
        */
        function(error){
          CompaniesFactory.companiesResult = null;
          CompaniesFactory.headerMessage = 'Could not find company with that stock symbol, please try again';
      }).$promise;
  };

  return CompaniesFactory;
}]);
