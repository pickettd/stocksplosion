stocksplosion = angular.module('stocksplosion', [
  'templates',
  'ngRoute',
  'ngResource',
  'controllers',
  'services',
  'googlechart'
]);

stocksplosion.config([ '$routeProvider',
  function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: "index.html",
        controller: 'CompaniesController'
    }
      );
  }]);

controllers = angular.module('controllers',[]);
services = angular.module('services',[]);
