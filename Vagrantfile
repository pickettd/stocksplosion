# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # Use Ubuntu 14.04 Trusty Tahr 64-bit as our operating system
  config.vm.box = "ubuntu/trusty64"

  # Configurate the virtual machine to use 2GB of RAM
  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "2048"]
  end

  # Forward the Rails server default port to the host
  config.vm.network :forwarded_port, guest: 3000, host: 3000

  # This shell script is run once to setup the VM.
  #config.vm.provision "shell", run: "always", privileged: false, inline: <<-SCRIPT
  config.vm.provision "shell", privileged: false, inline: <<-SCRIPT1
    export STOCKSPLOSION_DATABASE_ROLE="stocksplosion"
    export STOCKSPLOSION_DATABASE_PASSWORD="stocksplosion_password"

    sudo sh -c "echo 'deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main' > /etc/apt/sources.list.d/pgdg.list"
    wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -
    sudo add-apt-repository ppa:chris-lea/node.js 2>&1
    sudo apt-get update
    sudo apt-get install -q -y git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev nodejs ruby2.0 ruby2.0-dev postgresql-common postgresql-9.3 libpq-dev fontconfig

    sudo ln -sf /usr/bin/ruby2.0 /usr/bin/ruby
    sudo ln -sf /usr/bin/gem2.0 /usr/bin/gem

    echo "gem: --no-ri --no-rdoc" > ~/.gemrc
    sudo gem install bundler

    # In the development and testing environment a PG user is set up in this insecure way,
    # but in production we will be able to use environmental variables to set up secure access
    sudo -E -u postgres createuser $STOCKSPLOSION_DATABASE_ROLE -s
    sudo -E -u postgres psql -c "ALTER USER $STOCKSPLOSION_DATABASE_ROLE WITH PASSWORD '$STOCKSPLOSION_DATABASE_PASSWORD';"
    cd /vagrant
    bundle
    bundle exec rake db:create
  SCRIPT1

  # This shell script is run everytime the VM is started and will start the rails server
  config.vm.provision "shell", run: "always", privileged: false, inline: <<-SCRIPT2
    export STOCKSPLOSION_DATABASE_ROLE="stocksplosion"
    export STOCKSPLOSION_DATABASE_PASSWORD="stocksplosion_password"
    cd /vagrant
    bundle
    bundle exec rake db:migrate
    bundle exec rails s -b 0.0.0.0 -p 3000
  SCRIPT2
end
