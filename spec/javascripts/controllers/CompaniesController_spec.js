describe("CompaniesController", function() {
  var ctrl, httpBackend, location, resource, routeParams, scope, setupController, mockCompSvc;
  scope = null;
  ctrl = null;
  location = null;
  routeParams = null;
  resource = null;
  mockCompSvc = null;
  httpBackend = null;
  module(function($provide) {
    $provide.service('CompaniesFactory', function() {
      this.companiesResult = null;
      this.getAllCompanies = jasmine.createSpy('getAllCompanies').andCallFake(function() {
        this.companiesResult = [
          {
            id: 2,
            name: 'DXBT'
          },
          {
            id: 4,
            name: 'PHRL'
          }];
      });
      this.getOneCompany = jasmine.createSpy('getOneCompany').andCallFake(function(symbol, startDate, endDate) {
        this.companiesResult = [{id: 2, symbol: symbol, priceRows: []}];
      });
    });
  });
  setupController = function(symbol, results) {
    return inject(function($location, $routeParams, $rootScope, $resource, $httpBackend, $controller, CompaniesFactory) {
      scope = $rootScope.$new();
      location = $location;
      resource = $resource;
      routeParams = $routeParams;
      routeParams.symbol = symbol;
      mockCompSvc = CompaniesFactory;
      httpBackend = $httpBackend;
      if (results) {
        request = new RegExp("http:\/\/stocksplosion.apsis.io\/api\/company\/" + symbol+".*");
        httpBackend.expectGET(request).respond(results);
      }
      return $controller('CompaniesController', {
        $scope: scope,
        $location: location,
        CompaniesFactory: mockCompSvc
      });
    });
  };

  beforeEach(module("stocksplosion"));
  beforeEach(setupController());
  afterEach(function() {
    httpBackend.verifyNoOutstandingExpectation();
    return httpBackend.verifyNoOutstandingRequest();
  });

  describe('controller initialization', function() {
    describe('when no keywords present', function() {
      beforeEach(setupController());
      it('defaults to no recipes', function() {
        return expect(scope.companies).toEqualData(null);
      });
    });
    describe('with symbol', function() {
      var symbol, company;
      symbol = 'DXBT';
      company = {
        company: {
          id: 2,
          symbol: 'DXBT'
        },
          prices: []
      };
      beforeEach(function() {
        setupController(symbol, company);
        return httpBackend.flush();
      });
      it('calls the back-end and symbols match', function() {
        return expect(scope.companies[0].symbol).toEqualData(company.company.symbol);
      });
      it('calls the back-end and price arrays match', function() {
          return expect(scope.companies[0].priceRows).toEqualData(company.prices);
        });
    });
  });

  describe('search()', function() {
    beforeEach(function() {
      return setupController();
    });
    it('redirects to itself with a symbol param', function() {
      var symbol;
      symbol = 'DXBT';
      scope.search(symbol);
      expect(location.path()).toBe("/");
      return expect(location.search()).toEqualData({
        symbol: symbol
      });
    });
  });
});
