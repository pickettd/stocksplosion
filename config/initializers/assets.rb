# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w(
  teaspoon.css
  teaspoon-teaspoon.js
  teaspoon-jasmine.js
  support/bind-poly.js
  jquery.js
  jquery_ujs.js
  angular/angular.js
  angular-route/angular-route.js
  angular-resource/angular-resource.js
  angular-mocks/angular-mocks.js
  angular-google-chart/ng-google-chart.js
  main.js
  controllers/CompaniesController.js
  services/CompaniesFactory.js
  spec_helper.js
  controllers/CompaniesController_spec.js
)
